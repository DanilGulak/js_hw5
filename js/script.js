function createUser() {
    const newUser = {
        firstName: prompt('Enter your name'),
        lastName: prompt('Enter your surname'),
        userBirthday: prompt('Enter your birthday date', "dd.mm.yyyy").split("."),
        getLogin: function getLogin() {
            let login = (this.firstName[0] + this.lastName)
            return login.toLocaleLowerCase();
        },
        getAge: function getAge() {
            let currentDay = new Date().getDate(),
                currentMonth = new Date().getMonth(),
                currentYear = new Date().getFullYear(),
                currentDate = [],
                age;              // или age = 0; ?


            // currentDate = new Date(currentDay[0], currentMonth[1] + 1, currentYear[2])

            currentDate[0] = currentDay;
            currentDate[1] = currentMonth + 1;
            currentDate[2] = currentYear;


            age = currentDate[2] - this.userBirthday[2];
            if (this.userBirthday[1] - currentDate[1] > 0) {
                age -= 1;
            } else if (this.userBirthday[1] - currentDate[1] === 0) {
                age -= 1;
            }

            return (`Your age is - ${age}`);
        },
        getPassword: function getPassword() {
            return (`Your password is - ${this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.userBirthday[2]}`);
        }
    };
    return newUser;
}

let user = createUser();
console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());
